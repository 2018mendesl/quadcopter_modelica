within QuadcopterProject.Components;

model Chassis

parameter SI.Mass m_arm = 0.4 "Arm mass";
parameter SI.Mass m_body = 0.1 "GC mass";
parameter SI.Length l = 0.3 "Length";
parameter SI.Length l_m = l/2 "GC distance";
parameter SI.Length w = 0.02 "Width";
parameter SI.Density d = m_arm/(l*w^2) "Arm density";

  Modelica.Mechanics.MultiBody.Parts.BodyBox arm(density (displayUnit = "kg/m3") = d,r = {l, 0, 0}, width = w)  annotation(
    Placement(visible = true, transformation(origin = {30, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.PointMass body_center(m = m_body)  annotation(
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.BodyBox arm_2(density(displayUnit = "kg/m3") = d, r = {0,  0, -l}, width = w, widthDirection = {1, 0, 0}) annotation(
    Placement(visible = true, transformation(origin = {0, 32}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Modelica.Mechanics.MultiBody.Parts.BodyBox arm_3(density(displayUnit = "kg/m3") = d, r = {-l, 0, 0}, width = w) annotation(
    Placement(visible = true, transformation(origin = {-34, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Mechanics.MultiBody.Parts.BodyBox arm_4(density(displayUnit = "kg/m3") = d, r = {0, 0, l}, width = w, widthDirection = {1, 0, 0}) annotation(
    Placement(visible = true, transformation(origin = {0, -32}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_a GC "Gravity center" annotation(
    Placement(visible = true, transformation(origin = {-60, -32}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {0, 2}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b b_1 annotation(
    Placement(visible = true, transformation(origin = {68, 0}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {80, 0}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b b_2 annotation(
    Placement(visible = true, transformation(origin = {0, 82}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {0, 80}, extent = {{-16, -16}, {16, 16}}, rotation = -90)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b b_3 annotation(
    Placement(visible = true, transformation(origin = {-68, 0}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {-80, 2}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b b_4 annotation(
    Placement(visible = true, transformation(origin = {0, -62}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {0, -80}, extent = {{-16, -16}, {16, 16}}, rotation = -90)));    equation
  connect(arm_2.frame_b, b_2) annotation(
    Line(points = {{0, 42}, {0, 82}}, color = {95, 95, 95}));
  connect(arm_2.frame_a, body_center.frame_a) annotation(
    Line(points = {{0, 22}, {0, 0}}, color = {95, 95, 95}));
  connect(b_3, arm_3.frame_b) annotation(
    Line(points = {{-68, 0}, {-44, 0}}, color = {95, 95, 95}));
  connect(arm_3.frame_a, body_center.frame_a) annotation(
    Line(points = {{-24, 0}, {0, 0}}, color = {95, 95, 95}));
  connect(arm_4.frame_b, b_4) annotation(
    Line(points = {{0, -42}, {0, -62}}, color = {95, 95, 95}));
  connect(arm_4.frame_a, body_center.frame_a) annotation(
    Line(points = {{0, -22}, {0, 0}}, color = {95, 95, 95}));
  connect(arm.frame_a, body_center.frame_a) annotation(
    Line(points = {{20, 0}, {0, 0}, {0, 0}, {0, 0}}, color = {95, 95, 95}));
  connect(body_center.frame_a, GC) annotation(
    Line(points = {{0, 0}, {-12, 0}, {-12, -32}, {-60, -32}}, color = {95, 95, 95}));
  connect(arm.frame_b, b_1) annotation(
    Line(points = {{40, 0}, {68, 0}, {68, 0}, {68, 0}}, color = {95, 95, 95}));
annotation(
    Icon(graphics = {Rectangle(extent = {{-80, 4}, {-80, 4}}), Rectangle(extent = {{-82, 6}, {-82, 6}}), Rectangle(extent = {{-74, 4}, {-74, 4}}), Rectangle(extent = {{-76, 86}, {-76, 86}}), Rectangle(origin = {-39, 1}, fillColor = {0, 170, 0}, fillPattern = FillPattern.Solid, extent = {{-39, 3}, {41, -3}}), Rectangle(origin = {41, 1}, fillColor = {0, 170, 0}, fillPattern = FillPattern.Solid, extent = {{-39, 3}, {41, -3}}), Rectangle(origin = {1, 45}, rotation = -90, fillColor = {0, 170, 0}, fillPattern = FillPattern.Solid, extent = {{-39, 3}, {41, -3}}), Rectangle(origin = {0, -40}, rotation = -90, fillColor = {0, 170, 0}, fillPattern = FillPattern.Solid, extent = {{-39, 3}, {41, -3}})}));end Chassis;
