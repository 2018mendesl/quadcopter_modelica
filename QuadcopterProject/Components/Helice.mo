within QuadcopterProject.Components;

model Helice
  import Modelica.Constants.pi;
  final constant Real rho(displayUnit = "kg/m^3") = 1.2 "air volume mass";
  parameter SI.Diameter D = 0.254 "helice diameter";
  SI.Voltage v_supply "supply voltage (p.v - n.v)";
  /*Translational variables*/
  SI.Power Pu "thrust power";
  SI.Force T "thrust";
  SI.Velocity v "velocity along the rotor axis";
  /*Rotational variables */
  SI.Power P "mechanical power absorbed by the rotor";
  SI.AngularVelocity omega "rotor angular velocity";
  Real N(unit = "1/s") "rotor angular velocity in t/s";
  SI.Torque C "resistance torque on rotor axis";
  Real Ct "thrust coefficient";
  Real Cp "power coefficient";
  Real J "avance parameter v/N.D";
  
  /*Componentss*/
  Modelica.Mechanics.MultiBody.Forces.WorldForce forceHelice(N_to_m = 10, animation = true, resolveInFrame = Modelica.Mechanics.MultiBody.Types.ResolveInFrameB.frame_b) annotation(
    Placement(visible = true, transformation(origin = {-56, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_a frame_a "Coordinate system fixed at ... to be defined" annotation(
    Placement(visible = true, transformation(origin = {0, -82}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {0, -82}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Sensors.AbsoluteVelocity absoluteVelocity1(resolveInFrame = Modelica.Mechanics.MultiBody.Types.ResolveInFrameA.frame_a) annotation(
    Placement(visible = true, transformation(origin = {30, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput u "speed control [0,1]" annotation(
    Placement(visible = true, transformation(origin = {-92, -4}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.PositivePin p annotation(
    Placement(visible = true, transformation(origin = {-96, 64}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin n annotation(
    Placement(visible = true, transformation(origin = {96, 64}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.SignalCurrent motorCurrent annotation(
    Placement(visible = true, transformation(origin = {-4, 64}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.RealExpression iMotor(y = P / v_supply / 0.9) annotation(
    Placement(visible = true, transformation(origin = {-44, 88}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    
equation

  connect(iMotor.y, motorCurrent.i) annotation(
    Line(points = {{-33, 88}, {-4, 88}, {-4, 76}}, color = {0, 0, 127}));
  connect(p, motorCurrent.p) annotation(
    Line(points = {{-96, 64}, {-14, 64}}, color = {0, 0, 255}));
  connect(motorCurrent.n, n) annotation(
    Line(points = {{6, 64}, {96, 64}}, color = {0, 0, 255}));
  connect(forceHelice.frame_b, frame_a) annotation(
    Line(points = {{-56, -60}, {0, -60}, {0, -82}}, color = {95, 95, 95}));
  connect(absoluteVelocity1.frame_a, frame_a) annotation(
    Line(points = {{20, -40}, {20, -61}, {0, -61}, {0, -82}}, color = {95, 95, 95}));
    
/* Electical interface */
  v_supply = p.v - n.v;
 
/*Input from sensor0 = p.i + n.i;*/
  v = absoluteVelocity1.v[2];
  J = v / (N * D);
/*Rotor equations*/
/*Translational equations*/
  Ct = 0.1435 - 0.103 * J - 0.1173 * J ^ 2;
  T = rho * Ct * N ^ 2 * D ^ 4;
  Pu = T * v;
/*Rotational equations*/
  N = omega / (2 * pi);
  Cp = 0.0617 - 0.0238 * J - 0.11922 * J ^ 2;
  P = Cp * rho * N ^ 3 * D ^ 5;
  P = C * omega;
  omega = 6500 / 60 * 2 * pi * u "fixed rotational speed";
/*Output to force component*/
  forceHelice.force = {0, T, 0};
  annotation(
    Icon(graphics = {Rectangle(extent = {{-100, 100}, {100, -100}}), Text(origin = {0, -120}, extent = {{-100, 20}, {100, -20}}, textString = "%name"), Polygon(fillColor = {255, 170, 0}, fillPattern = FillPattern.Solid, points = {{-80, 20}, {-80, -20}, {80, 20}, {80, -20}, {-80, 20}})}));
end Helice;
