within QuadcopterProject;

model TestDrone
  QuadcopterProject.Drone drone1 annotation(
    Placement(visible = true, transformation(origin = {0, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  inner Modelica.Mechanics.MultiBody.World world annotation(
    Placement(visible = true, transformation(origin = {-90, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const(k = 1)  annotation(
    Placement(visible = true, transformation(origin = {-88, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(drone1.u_2, const.y) annotation(
    Line(points = {{10, 2}, {22, 2}, {22, 22}, {-76, 22}, {-76, 4}, {-76, 4}, {-76, 4}}, color = {0, 0, 127}));
  connect(drone1.u_3, const.y) annotation(
    Line(points = {{0, -8}, {-64, -8}, {-64, 4}, {-76, 4}, {-76, 4}}, color = {0, 0, 127}));
  connect(drone1.u_1, const.y) annotation(
    Line(points = {{0, 12}, {-58, 12}, {-58, 4}, {-76, 4}, {-76, 4}}, color = {0, 0, 127}));
  connect(const.y, drone1.u_4) annotation(
    Line(points = {{-76, 4}, {-10, 4}, {-10, 2}, {-10, 2}}, color = {0, 0, 127}));
end TestDrone;
