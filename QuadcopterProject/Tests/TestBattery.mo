within QuadcopterProject.Tests;

model TestBattery
  QuadcopterProject.Components.Helice helice1 annotation(
    Placement(visible = true, transformation(origin = {0, 12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  inner Modelica.Mechanics.MultiBody.World world annotation(
    Placement(visible = true, transformation(origin = {-70, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  QuadcopterProject.Components.Chassis chassis1 annotation(
    Placement(visible = true, transformation(origin = {1, -51}, extent = {{-23, -23}, {23, 23}}, rotation = 0)));
  QuadcopterProject.Components.Battery battery1 annotation(
    Placement(visible = true, transformation(origin = {2, 64}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const(k = 1)  annotation(
    Placement(visible = true, transformation(origin = {-48, 12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground1 annotation(
    Placement(visible = true, transformation(origin = {58, 48}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(helice1.n, battery1.pin_n) annotation(
    Line(points = {{12, 6}, {12, 6}, {12, 64}, {12, 64}}, color = {0, 0, 255}));
  connect(ground1.p, battery1.pin_n) annotation(
    Line(points = {{58, 58}, {56, 58}, {56, 64}, {12, 64}, {12, 64}}, color = {0, 0, 255}));
  connect(helice1.u, const.y) annotation(
    Line(points = {{-12, 12}, {-37, 12}}, color = {0, 0, 127}));
  connect(helice1.frame_a, chassis1.GC) annotation(
    Line(points = {{0, 4}, {0, -21.5}, {1, -21.5}, {1, -51}}, color = {95, 95, 95}));
  connect(battery1.pin_p, helice1.p) annotation(
    Line(points = {{-8, 64}, {-20, 64}, {-20, 2}, {-12, 2}, {-12, 5}, {-11, 5}}, color = {0, 0, 255}));
  connect(battery1.GC, chassis1.GC) annotation(
    Line(points = {{2, 60}, {28, 60}, {28, -38}, {10, -38}, {10, -50}, {2, -50}}, color = {95, 95, 95}));
end TestBattery;
