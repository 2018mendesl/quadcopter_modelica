within QuadcopterProject.Components;

model Battery
  Real C (unit = "mAh")"Charge" ;
  final constant SI.Voltage Vbat = 1.5 "AA battery tension" ;
  final constant SI.Mass Mbat = 0.1 "AA battery mass";
  constant Real  n_bat = 5 "number of batteries";
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_p annotation(
    Placement(visible = true, transformation(origin = {-98, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-98, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_n annotation(
    Placement(visible = true, transformation(origin = {98, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {98, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.PointMass BatteriesMass(m = Mbat * n_bat)  annotation(
    Placement(visible = true, transformation(origin = {0, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_a GC annotation(
    Placement(visible = true, transformation(origin = {0, -84}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {0, -84}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor1 annotation(
    Placement(visible = true, transformation(origin = {-50, 46}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Blocks.Continuous.Integrator integrator1 annotation(
    Placement(visible = true, transformation(origin = {-26, 82}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.SignalVoltage signalVoltage1 annotation(
    Placement(visible = true, transformation(origin = {-2, 46}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.RealExpression realExpression1(y = Vbat * n_bat)  annotation(
    Placement(visible = true, transformation(origin = {28, 78}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
equation
  connect(BatteriesMass.frame_a, GC) annotation(
    Line(points = {{0, 2}, {0, -84}}, color = {95, 95, 95}));
  connect(realExpression1.y, signalVoltage1.v) annotation(
    Line(points = {{16, 78}, {-2, 78}, {-2, 58}}, color = {0, 0, 127}));
  connect(pin_n, signalVoltage1.n) annotation(
    Line(points = {{98, 0}, {94, 0}, {94, 46}, {8, 46}}, color = {0, 0, 255}));
  connect(currentSensor1.p, signalVoltage1.p) annotation(
    Line(points = {{-40, 46}, {-12, 46}}, color = {0, 0, 255}));
  connect(currentSensor1.n, pin_p) annotation(
    Line(points = {{-60, 46}, {-98, 46}, {-98, 0}}, color = {0, 0, 255}));
  connect(currentSensor1.i, integrator1.u) annotation(
    Line(points = {{-50, 57}, {-50, 82}, {-38, 82}}, color = {0, 0, 127}));
  C = integrator1.y * 1000 / 3600;
annotation(
    Icon(graphics = {Rectangle(origin = {-2, 0}, fillColor = {181, 181, 181}, fillPattern = FillPattern.Solid, extent = {{-78, 72}, {82, -74}}), Polygon(points = {{-38, 30}, {-38, 30}, {-38, 30}, {-38, 30}, {-38, 30}, {-38, 30}, {-38, 30}}), Rectangle(extent = {{-64, 64}, {-64, 64}}), Rectangle(extent = {{-66, 64}, {-66, 64}}), Rectangle(origin = {4, 2}, fillColor = {255, 0, 0}, fillPattern = FillPattern.Solid, extent = {{-62, 64}, {-56, 46}}), Bitmap(extent = {{-42, 64}, {-42, 64}}), Rectangle(fillColor = {255, 0, 0},fillPattern = FillPattern.Solid, extent = {{-66, 60}, {-44, 54}}), Rectangle(extent = {{40, 60}, {40, 60}}), Rectangle(extent = {{38, 62}, {38, 62}}), Rectangle(extent = {{44, 62}, {44, 62}}), Rectangle(extent = {{18, 54}, {18, 54}}), Rectangle(extent = {{-16, 76}, {-16, 76}}), Rectangle(extent = {{-18, 58}, {-18, 58}}), Rectangle(extent = {{48, 64}, {48, 64}}), Rectangle(extent = {{48, 62}, {48, 62}}), Rectangle(origin = {120, 0}, fillPattern = FillPattern.Solid, extent = {{-76, 60}, {-54, 54}}), Rectangle(origin = {-83, 0}, extent = {{-3, 8}, {3, -8}})}, coordinateSystem(initialScale = 0.1)));end Battery;
