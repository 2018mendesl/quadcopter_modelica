within QuadcopterProject.Tests;
  
  
  
model TestHelice "Helice component test"

  Real C (unit = "mAh")"Charge" ;
  QuadcopterProject.Components.Helice helice1 annotation(
    Placement(visible = true, transformation(origin = {0, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  inner Modelica.Mechanics.MultiBody.World world annotation(
    Placement(visible = true, transformation(origin = {-70, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  QuadcopterProject.Components.Chassis chassis1 annotation(
    Placement(visible = true, transformation(origin = {1, -51}, extent = {{-23, -23}, {23, 23}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const(k = 1)  annotation(
    Placement(visible = true, transformation(origin = {-50, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor1 annotation(
    Placement(visible = true, transformation(origin = {-20, 38}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Blocks.Continuous.Integrator integrator1 annotation(
    Placement(visible = true, transformation(origin = {-68, 38}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage1(V = 10)  annotation(
    Placement(visible = true, transformation(origin = {0, 68}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground1 annotation(
    Placement(visible = true, transformation(origin = {68, 58}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  
equation
  connect(helice1.u, const.y) annotation(
    Line(points = {{-12, 10}, {-39, 10}}, color = {0, 0, 127}));
  connect(helice1.n, constantVoltage1.n) annotation(
    Line(points = {{12, 4}, {10, 4}, {10, 68}}, color = {0, 0, 255}));
  connect(ground1.p, constantVoltage1.n) annotation(
    Line(points = {{68, 68}, {10, 68}}, color = {0, 0, 255}));
  connect(currentSensor1.p, constantVoltage1.p) annotation(
    Line(points = {{-20, 48}, {-20, 48}, {-20, 68}, {-10, 68}, {-10, 68}}, color = {0, 0, 255}));
  connect(currentSensor1.i, integrator1.u) annotation(
    Line(points = {{-31, 38}, {-56, 38}}, color = {0, 0, 127}));
  connect(helice1.frame_a, chassis1.GC) annotation(
    Line(points = {{0, 2}, {0, -21.5}, {1, -21.5}, {1, -51}}, color = {95, 95, 95}));
  connect(helice1.p, currentSensor1.n) annotation(
    Line(points = {{-10, 4}, {-20, 4}, {-20, 28}, {-20, 28}}, color = {0, 0, 255}));
  C = integrator1.y * 1000 / 3600;
end TestHelice;
