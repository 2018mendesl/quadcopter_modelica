within QuadcopterProject.Tests;

model TestChassis
  QuadcopterProject.Components.Chassis chassis1 annotation(
    Placement(visible = true, transformation(origin = {-1, -1}, extent = {{-41, -41}, {41, 41}}, rotation = -90)));
  inner Modelica.Mechanics.MultiBody.World world annotation(
    Placement(visible = true, transformation(origin = {-80, -68}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Forces.WorldForce force2 annotation(
    Placement(visible = true, transformation(origin = {64, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Mechanics.MultiBody.Forces.WorldForce force annotation(
    Placement(visible = true, transformation(origin = {0, 62}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Mechanics.MultiBody.Forces.WorldForce force1 annotation(
    Placement(visible = true, transformation(origin = {0, -58}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Modelica.Mechanics.MultiBody.Forces.WorldForce force3 annotation(
    Placement(visible = true, transformation(origin = {-62, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant f[3](k = {0, 5, 0})  annotation(
    Placement(visible = true, transformation(origin = {70, -38}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(f.y, force3.force) annotation(
    Line(points = {{82, -38}, {128, -38}, {128, 98}, {-96, 98}, {-96, 0}, {-74, 0}, {-74, 0}}, color = {0, 0, 127}, thickness = 0.5));
  connect(f.y, force.force) annotation(
    Line(points = {{82, -38}, {116, -38}, {116, 92}, {0, 92}, {0, 74}, {0, 74}}, color = {0, 0, 127}, thickness = 0.5));
  connect(f.y, force1.force) annotation(
    Line(points = {{82, -38}, {92, -38}, {92, -84}, {0, -84}, {0, -70}, {0, -70}}, color = {0, 0, 127}, thickness = 0.5));
  connect(f.y, force2.force) annotation(
    Line(points = {{82, -38}, {96, -38}, {96, 0}, {76, 0}, {76, 0}}, color = {0, 0, 127}, thickness = 0.5));
  connect(force3.frame_b, chassis1.b_4) annotation(
    Line(points = {{-52, 0}, {-34, 0}, {-34, 0}, {-34, 0}}, color = {95, 95, 95}));
  connect(force1.frame_b, chassis1.b_1) annotation(
    Line(points = {{0, -48}, {0, -48}, {0, -34}, {0, -34}}, color = {95, 95, 95}));
  connect(force.frame_b, chassis1.b_3) annotation(
    Line(points = {{0, 52}, {0, 52}, {0, 32}, {0, 32}}, color = {95, 95, 95}));
  connect(chassis1.b_2, force2.frame_b) annotation(
    Line(points = {{32, 0}, {54, 0}}, color = {95, 95, 95}));
end TestChassis;
