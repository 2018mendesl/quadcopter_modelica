within QuadcopterProject;

  model Drone
  QuadcopterProject.Components.Helice helice1 annotation(
    Placement(visible = true, transformation(origin = {16, 54}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  QuadcopterProject.Components.Chassis chassis annotation(
    Placement(visible = true, transformation(origin = {-1, -5}, extent = {{-19, -19}, {19, 19}}, rotation = 0)));
  QuadcopterProject.Components.Helice helice2 annotation(
    Placement(visible = true, transformation(origin = {54, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  QuadcopterProject.Components.Helice helice3 annotation(
    Placement(visible = true, transformation(origin = {-16, -46}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  QuadcopterProject.Components.Helice helice4 annotation(
    Placement(visible = true, transformation(origin = {-46, 12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  QuadcopterProject.Components.Battery Battery annotation(
    Placement(visible = true, transformation(origin = {-36, 58}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground Ground annotation(
    Placement(visible = true, transformation(origin = {-62, 32}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput u_1 annotation(
    Placement(visible = true, transformation(origin = {0, 100}, extent = {{-20, -20}, {20, 20}}, rotation = -90), iconTransformation(origin = {0, 100}, extent = {{-20, -20}, {20, 20}}, rotation = -90)));
  Modelica.Blocks.Interfaces.RealInput u_2 annotation(
    Placement(visible = true, transformation(origin = {100, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 180), iconTransformation(origin = {100, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 180)));
  Modelica.Blocks.Interfaces.RealInput u_3 annotation(
    Placement(visible = true, transformation(origin = {0, -100}, extent = {{-20, -20}, {20, 20}}, rotation = 90), iconTransformation(origin = {0, -100}, extent = {{-20, -20}, {20, 20}}, rotation = 90)));
  Modelica.Blocks.Interfaces.RealInput u_4 annotation(
    Placement(visible = true, transformation(origin = {-100, 2}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-100, 2}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
equation
  connect(helice4.u, u_4) annotation(
    Line(points = {{-58, 12}, {-78, 12}, {-78, 2}, {-100, 2}}, color = {0, 0, 127}));
  connect(helice3.u, u_3) annotation(
    Line(points = {{-16, -58}, {0, -58}, {0, -100}, {0, -100}}, color = {0, 0, 127}));
  connect(u_2, helice2.u) annotation(
    Line(points = {{100, 0}, {66, 0}}, color = {0, 0, 127}));
  connect(helice1.u, u_1) annotation(
    Line(points = {{4, 54}, {0, 54}, {0, 100}}, color = {0, 0, 127}));
  connect(Battery.pin_p, helice4.p) annotation(
    Line(points = {{-46, 58}, {-92, 58}, {-92, -18}, {-66, -18}, {-66, 2}, {-57, 2}, {-57, 5}}, color = {0, 0, 255}));
  connect(Battery.pin_n, helice4.n) annotation(
    Line(points = {{-26, 58}, {-26, 5}, {-35, 5}}, color = {0, 0, 255}));
  connect(chassis.b_3, helice4.frame_a) annotation(
    Line(points = {{-16, -4}, {-46, -4}, {-46, 4}}, color = {95, 95, 95}));
  connect(Battery.pin_n, Ground.p) annotation(
    Line(points = {{-26, 58}, {-26, 42}, {-62, 42}}, color = {0, 0, 255}));
  connect(Battery.pin_n, helice3.n) annotation(
    Line(points = {{-26, 58}, {-26, 58}, {-26, -28}, {-10, -28}, {-10, -34}, {-8, -34}}, color = {0, 0, 255}));
  connect(Battery.pin_n, helice2.n) annotation(
    Line(points = {{-26, 58}, {-20, 58}, {-20, 20}, {28, 20}, {28, 8}, {44, 8}, {44, 8}}, color = {0, 0, 255}));
  connect(Battery.pin_p, helice3.p) annotation(
    Line(points = {{-46, 58}, {-92, 58}, {-92, -96}, {28, -96}, {28, -58}, {-8, -58}, {-8, -56}}, color = {0, 0, 255}));
  connect(Battery.pin_p, helice2.p) annotation(
    Line(points = {{-46, 58}, {-46, 58}, {-46, 90}, {66, 90}, {66, 8}, {66, 8}}, color = {0, 0, 255}));
  connect(Battery.pin_p, helice1.p) annotation(
    Line(points = {{-46, 58}, {-46, 58}, {-46, 38}, {2, 38}, {2, 46}, {6, 46}, {6, 48}}, color = {0, 0, 255}));
  connect(Battery.pin_n, helice1.n) annotation(
    Line(points = {{-26, 58}, {-24, 58}, {-24, 88}, {34, 88}, {34, 48}, {28, 48}, {28, 48}, {28, 48}}, color = {0, 0, 255}));
  connect(Battery.GC, chassis.GC) annotation(
    Line(points = {{-36, 48}, {-16, 48}, {-16, 4}, {-2, 4}, {-2, -4}, {0, -4}}, color = {95, 95, 95}));
  connect(helice1.frame_a, chassis.b_2) annotation(
    Line(points = {{16, 46}, {16, 34}, {0, 34}, {0, 10}}, color = {95, 95, 95}));
  connect(helice2.frame_a, chassis.b_1) annotation(
    Line(points = {{54, 8}, {54, 14}, {34, 14}, {34, -4}, {14, -4}}, color = {95, 95, 95}));
  connect(helice3.frame_a, chassis.b_4) annotation(
    Line(points = {{-8, -46}, {-1, -46}, {-1, -20}}, color = {95, 95, 95}));
  annotation(
      Icon(graphics = {Bitmap(origin = {-7, 5}, extent = {{-87, 89}, {101, -99}}, imageSource = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAsJCQcJCQcJCQkJCwkJCQkJCQsJCwsMCwsLDA0QDBEODQ4MEhkSJRodJR0ZHxwpKRYlNzU2GioyPi0pMBk7IRP/2wBDAQcICAsJCxULCxUsHRkdLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCz/wAARCACwATkDASIAAhEBAxEB/8QAHAABAAEFAQEAAAAAAAAAAAAAAAEDBAUGBwII/8QAQxAAAQMDAgQEBQEFBAcJAAAAAQACAwQFERIhBhMxQSJRYXEUMoGRoQcjQlJysRUkwdElU2KCotPxM0Njc5KVsrPC/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/xAAWEQEBAQAAAAAAAAAAAAAAAAAAESH/2gAMAwEAAhEDEQA/AOtoiICIiAig57Lw57x2QVEVAzkdl5+KaOoQXKK3FXAersH1VZr2P3ac+26D0iguaDuce+ykEFAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQFKhSghERAREQNk2REHkxsd1CoPpmu6EfZXKboMc+geejiM+R/zWIvMtJZqUVFXWyQiWQQxtjhfK6Rx3+WPfA7k7LHyfqJb45p4zbazlsleyOZz49EjWuLQ/Eepwz16d1r3E/ENLfoaQR1FFTCITNYJasYe6QtBy4tGMYxghBmqW+wi11U0dzifVhsr44nyt1sJJ0t0vPQK7iu3ETIopJKNk8b2gieBxc1xAGdmf5LjFDT3Cvqnw0MEtVKA95jgDHnS0gEjJ9Rhdx4Ht9bScPwRV8M0M76mql5NQ0sfEwv0tboPTOM/VBUp7/VOxzISD3GDt991lYbnHIBqZg+irvoaZ/Voz7BUv7OjHyn22QXbJoX9HD67KpsVZCje3o4Ks2OZvfZBX2RQNXdSgIvL5I426pHsY3IGp7g0ZPQZKhssLvlljd/K9p/oUHtEByMjoiAiIgImR3I+6ah5j7oCJn1/KICIiAiIgIiICIiAiIgKVClBCIiAiIgKCQE9lQeXDPX7IKpk8lbVdSYaSunBxyKWomz5cuNzsry5zt+qwnEtS6Dh+/ydC6jMA36mZ7WY/qg5ZSVLKiJr8YzkFp/dc3Yj/Ee/ok8MLtWqNriRt4RlY2hk5ENXORnBja1pOAXdP8d1Xop6+urKGijjhdLVzxwMOHYaHHLnnB6NAJ+iCh8PGxxPwzWuG7Xtc7Iz5gELtPCVyoZbLZqU3CGetp6SGGoY6Qc8SNHQtfhxx0ytBq+G7nASYDFVR52MR0SEf+XIf6OKxJjkhkEcsUkcgOzXtcx4/lzg/ZB3fW3oTg+RGCvWQe65FRX+/wBCGtirJJGN6RVY5rMeQ17j7rYqTj2FmG3KifGdgZaQ8xvuY37/AJQb2ixVBf7FcsfB19PI4jeNzuXKP9x+CsoT5hBKJsVDnNY1znHDWgucT0AAySg5zxbV1MtzlZzCaal0RRMHysk0hz3Y/iJPX0WOor1JCWNmcSB8sgHibjzVWteaySedrZHc+WWTBAef2zg1udsdzha9U4a97mNDWO8QYCXaM7huXb7IOhw36VwZkxYIBEjGlpx5nQcH7LMU96hcWtlIdkfPGCPuCAuR09wlg8GpxYfI7s36j/FZyGvdhrmluDuCHHH9EHVo5YpW6o3hw9D0916LQ4Y3+i57R3iohc1zHkEf7QIPuCtrt18pastimxFOdmkn9m/2J6H0ygvpaec/JJn0Ox+6x08NxZnAf7jcfhZtEGpSz3aPOku29Mq3/tq/wHJpnStB6Brg7/hW5lkbvma0+4Cpmmp/4Gj2QazT8VPdPRU01urYpKmQRtMmlrcnPiGRkhZaW+W+nqRSzmVshjbIXBgdGA4loyQc/hct4xvV6t/EFVLH8VAIJuXRF7HxtEbGhuqMSN0EHffCvOGLtd7perZNWPiqBUuFLKyWCIZjbG92ppYMbAeSDp7bpbn/ACzh3sHD+oVZtVSv6SN+qtnWqiJyyJrf5Nvx0UC3hp8Or7oMgHNOMEH2IUqybTvb/Eq7RIO7vqgrIoGe6lAREQFKhEBERAREQF5c0FekQW0kYWocdahYnQM+erq4mNHTPLY+Qj+i3hYO/wDD7L7FRxGrkphTSSyfs42Say9mjfUeyDhG4oItgHT1UpxjfDPButj4GofirxXVWkllvoyxp7CapPLGPXSHfdbFP+mlxnnhzd6SOmhB0FlHK6R5cS4lzDIGg/VbHw9wrBw5TVkEdTLVS1VQyeaaWNkZOlmhrWtYSMDc9e5QeX0j3dM+6w14+Hp43RT1EZk0BzInGFzxq+U6XZ/ot2+HBwOxcAfYlcg4kMLrtWz4c2Z9RKXg+E7OIbqa7vjCDPzWmjdSQTRS1Ands5scrZYXHuSyTcfRwVrNwzdTHzmRCdnnTHU4d943kH7ZWLpKipkifG2U7436OGP9obrofB01XPaZPipObyqyaGF7h4yxobnJ77k4Qc2moHteWnWyRvVrmvjkb7t2d+Fe0d64qtWkU1fM+Jp/7GqBlZjy8e66xUUFFWM0VNPDO3faVgc5vsfmH0K16t4Lt8wc6hqJqV56Ryf3iDPlhxEg/wDWgxVF+oj26WXW3Pb0DpaTJb7lpys3UcU2OvtdxFBV66iSB0TIXNc2XMmIyQPTJP0Wn3DhXiKjDnfCfFRDfmUJ5u3rGQJPs0rXJIW6iNJbIw4IGWSMPkRs4IMxI9zQ5gzpIIb1AB81ZSBjzK4yMDWuLSSQANO25Oyxsgr2yNlirZtTP3ZiZGkeRBOF4hqJopHc051kiXOHA56OQVJQGOy0hzHbhzSC0+xGy909U6J2kk6HH7FephrZpGkDHh0gAD2wrOCFhkxNu1h3Z/H7+nmgz0dXTjGqcB2+QNwB5nB6f5K/grocgNqot9/Fqb3xjcfX6LDGrj2aCwdAAC0Y2/hC9NqWnJLmYBaNLs63ajjw7Y27oOl2W/lwhpq17S2Q6Keo1tcNQ2DJSDtnsT/itpXFG6cF8fhPnGdJ/Cz9n4vvEEjKaocKuNnzc5zGScsbZa7GSR07+yDpit66qjoaOurZMaKSmmqHZOARGwux9ei1+r4xt0MeqCKR50gl05ETGk+gy4/Zahd+MblWxywRva2GQYe2KNoa4Ht48uwg0WuqK2rdIauprJxPOKqSOV8krJJsE6g0n17LMWuvrbOxlRSVEby4nQx1MIpKV+nBIf8AMQRt1IVs6Rznufgl7ju45LvyqMjpZcwRNfzp3RU0Ph2dJNIIWj7lB27hesuNwsdsrbg9r6mqZJNqDBHmJ0juUS1u2dOD0WaVtSwR0dNSUjPkpYIadmNvDGwM6fRVSfIoKiKlqcFIkx1QVEXjmtXrU3zCCUREBSoUoIRNk2QETZNkBE2TZARNk2QFrHFt/rrHDQCiip5J6t84JqQ9zGRxNGSGsI3JIxv9Ctn2XLP1KrporvZKWMtLW2+eZ7XD/WS6Rv8A7qCxn4z4mlaebVywDsKaOJrPo9jdX5WoVlZVVdTLNPUVE8j3l73PzK7J7Z+ZXrKyJ3zgtPmNx9xuq1SOWyGMDTn9ocDBLneZ6oKNHVSQMcA0aiMNMuWt+vddDtfFdgoKGlpoaO5EMa5ziDFJqkkJe52okEjPTZc2ONEzjsIwHEn1z3WxwUN8jgp2CyXU6Io25FvqCDgdQQEG5N47tDgdNBcsjbBbED9PEvJ/UK1Alpt1wBHTXyhn65WpfCXvvY7t/wC3Vf8AgxU3Ul2OQ6y3b622t/5aDa3/AKj22M4Nqr/Q8ynAP11LF3Hjvhe4NLa/hqoqMDDXufS8xv8ALI1wePoVg3Udw3Bs90A9bbW/8pW77fVd7VcR5Ztldn/6UFheLrw7JofaobvAS13MgrTTyxh2dhHMH8zGPPKtWMlfFFM5jg2RrS1zi0hxcM4GCvN3iqoG04bHNRHL9Zq6aanEjTgNLebGDtuFjxLNJG4Syse+N7HxyQyRN04By7Bwdu2P+oZ2BxI0Ht09WrH3mGqZE2oie4Q5Dahrcjc7NcSN8Hp/1Vzb3vqJYoThs+xGBkPaf+8YO481keIKP4S0VBDpXunmggALWtGkEyuwOvYINcsdpN5nlhFS2FzAwsAYHPfk7nBI2HddHt/BFljYwTPrah4AyX1DmNOfJsWBhaLwrXQ0NzozkcmrlbSTvfo1Rl+zHB3YA4zv39F2aKVkTA55wMem/fbsgx8HC1gjyI6N4yMH+8VBJ+71rHFtj/sBtFdqOR/w76gQOZO7W6CYtc5uHY3Y4Bw33HTJytkqOLqOnkLIKaoqdJLXmBuWgjrl/RWfEF9tHEXCXEcMLXNqqWngrTTztAkDIaiNxkYRsRjIOPP13DU46qku1O8AGOVm7mZy6J52Dmnu0rZOG+BYK6GnrrlcGSwPJIpLfloy07snmd4sjoQ0D3XM7fJNDNHNTvL4xtk5OG945R5LfuHuIjbqjdz/AIWoAZWQAgvaDsJYg7Yub+R+A1zVEKi7SRhrY4/iBC3U7DG6y1g3+iy3ClJ8ffeHY3gPFPLVXOY5B8FG3THnb+Nzft6K5fwpbJWVTLHxPQTumLMQ3X9hM3S7UQHAAnP8i2jgnhy62qa41dyigje+mpqOl5ErJg6MPfNK8FnQOJbj2Qbc4yD1VJ0so/dKvcBecN8ggx8lc6GOR743ENaTk7DPQZKtaG7x1zalzWlnIldE/UMeJuCcLE8d3KsoqKhpqScwvq5JXTFobqMUQbhueu5P4Wj0nEl1pYH07MPEjJATlpJc/bJyM59ig6Sy90U4LoJQ9oJBLfQ46qoLo0nAz+Fp3DEAr6KeOWCaKSkMQbI799jw7GO/ZZv4CaM+F+R2QbBFcmnG6vGVcbsbjK1hkdQ3Gc/ZXkWsY6hBsbXtcMgr1kLERSSDG6r85/mUGQREQEREBERAREQCuK8fzGbiysZnIpaGihHpqbzT/wDJdqPkuBcSzc/ibiaTOcV74AfSFoi/wQWFPGZKiBnYvBPsN1bz1j3Xi4syTDkx+ejkNDNTfrlZG3ANfPO47Qx9e2Tv1+iqcHW1l04ktrHxgtErqypyDgxwnnkOx5nSPqg3/h/gGhNJTVN8Ess8uic0IeY4IR8zY5tHic7pq8WM7YOMnoA6D2REBERAREQajxpYzdaemkZTS1L49cMkUY1EscdQOnIPXrg9/Rc3n4UpoTiez1sROerasD32JC7siDgps8XKY2GGpjip8MDxFM8aiS8hzsZzv5q1uNA91OwtqXyCneZH075XM1gjxPDpXEagOueo2X0Iqb4KeUESQxPBGCHsY4EeoIQaDG79KY4aeSWO3c2WKJzW/CyvfgtGlmlrMd8KnxJKylt5FJHyGVE0dM0bt0gtLnZyTjOAFlLjwHZKmr/tIPrjLDM2ohpGyxtpPDJzeWGBmcZ369lbXe3tudDPSl7Wy6+Ywuzhszc4Dh9wff0QcqM0bnPE80zdEjgGQu2DgdO2+Pc4VxC5jpYImzaI6yOSnc95dltPMXRF0hYC7BAyfCdlWq7NxEyc82zvqpdeedvIx+BpGrQ8Rke491tnCnAVTO99wvpfHkN5NPA4DYkZ1vb4dx4cN6DYH+G3Em1qN54S4i4cDa9rGuoJNIE9NLz4cOAI5+ljSGnsdOPUHrFqq4ZdpWFsrw5oDQJWEhuSW6MnGN9xsvoJkbGMbE1oEbGCNrcbBoGA3B7K3it1rgmNTDQUcVQQ5pmigiZLh3Ua2tB37qK4fPTUlRK90ZjJwwfs5AHbDHTIP4UxG70JzSV1dTEYwI5pGD7dF3Cot1rqwRVUNJPn/XQRP/LgsTNwfwvLq0UTqdzjnNHPPCPoxrtH/Cg5xBxfxzSbfHMqWjtVQRv+7mgO/KykH6mXaIgV1mppQOppZpIXHHU4kDh+VsE/ANC45prjVx7dKiOCdufcNY78rE1HAN5bnkVFunHk7n07j9xI1BrXE3FMnEslE6iohBHSxOBbOGOmdJIcuBkYflGBp+ufTX6eaZsoE8Rjdn95wDT6aisg23VNvfLBVxOiqWSSNkjkBa4EEjIz1B7EL2WscC1wB2BIPkfdBvXDtzstHSvZPNK2WokEr5TFrgAa0MawOjJdtv27raYTRVbC+lqIZ2jqYXtcR/MBuPsuMth5RLqeWWF3/huc0fVo2/CznDQqZeKrFA+ZzjCJamQjDS4R0zichuNsuCDpvw/opFP3wsnpb5KNDUFi2PHZe+X6K70BTpCCVKhEBSoRAREQEREAkDc9Bkk+y+cayX/SVzdI/erqpauIu8Jk57y4hueuDsvoyRutkjMlutrm5HUZGM7rllw4Ev8ARgvo3QXGFhLmtAbHUDAznly+An2cPZBqRinjttS1rCJ5WvOnO+Nm429M4W3/AKVUOp16uZHythoIfd37aT/8BWVLwfxbdozHLTxW2nOAZa7JmODnwQRu1fctXR+G7Gzh62R28VBqZObLPPOYmxcySQjfltJAAAAG56IMyiIgIiICIiAiIgIiICpPp6aU5kghefN7GuP3IVVEFFlLSRnMdPA0+bY2A/cBVkRAUqEQEREBERBa1tvt1wj5VbTQzx4OBK0Et9WOHiB9iFy7iqyQWKpY6J0ho5oZqiEvOp8XK3kj1Hr2xnz3811tc8/U17W0ltG2eVX5Ho/ksQc/paiSrdC9rTEx0gLWg6nkDfxuP+S2vgCDn8WcQVZB00lC6Fh7B00zWf0YVrNqjANPts1oJ+pW8/pbAXU3E1wIH96uUcDT6QR8w/mRB0VSoRAUqFKCEREBERAREQEREBERAx6BPsiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAuX/qi6d7qCGFhe8UurAIAAfP1JO2PCuoLD3fhyw3ssdcKXXKyPlxzxyPimY3JIaHsI2ySd8oOO28PjpppnlhLYZCAwlwBjY7944HVdL/TmmdTcJ2x7gA+skqq12M782Vwb19AFhav9PbqyT4e23SIUE+qN8lVGTU00bvmLBGA1x7Ddvqt/t1DBbaCgt8GeTR00NNGXY1ObGwN1Oxtk9T7oLpSoRAUqFKCEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBSoRAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQFKhSghERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAUqFKD/9k=")}));
  end Drone;
