# Modelica QuadcopterProject

The modelica QuadcopterProject is a model of a quadrotor drone with a simple cross shape body.

Author: Arthur Gérard, Yihan Lin, Leone Mendes, Cesar Peinado

The code is organized in the `QuadcopterProject` package which contains:

* the simulation model `TestDrone`
* the main component of the simulation : `Drone`
* a subpackage `Components` for the subcomponents of the drone: `Chassis`, `Helice`and`Battery`.
* a subpackage `Tests` for standalone testing of some complex components:`TestHelice`, `TestChassis`and`TestBattery`.

The project is hosted in the public repository https://gitlab-student.centralesupelec.fr/2018mendesl/quadcopter_modelica

## Design principles

The drone was designed based on a very simple model of a chassis which has a symmetric cross shape which has helices in each branch.
The helice was modeled based on a simplification of an existent '10"x4.7" Slow Flyer' helice which can be found here: http://éole.net/courses/modelica/63-project-quadcopter.html. 

Here are some design considerations:
* The 4 helices are fixed at the end of each chassis arm
* Each helice turn in the same direction as its opposite
* The helice will always turn in 6000 rpm
* A battery block which can change the number of battery in series in order to supply the desired tension
* Each battery has also a mass which is referenced in the middle of the châssis

Here are some principles I targeted during the development.

* The package structure is reasonably "tidy"
* All models have a description string
* All parameters and variables have a physical unit and, when applicable, 
  a reasonable displayUnit
