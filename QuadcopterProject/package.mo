package QuadcopterProject "Root package of the Quadcopter modeling project"

  /*Import of useful physical types*/
  import SI = Modelica.SIunits;

  annotation(
    uses(Modelica(version = "3.2.3")));
end QuadcopterProject;
